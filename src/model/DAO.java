/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Suiken
 */
public class DAO {

    public static List<String> getDepartement() {

        String url = "http://gaemedecins.appspot.com/Controleur/medParDep/listeDep";
        URL myURL;
        List<String> lesDepartements = new ArrayList<String>();
        Document doc;

        try {
            myURL = new URL(url);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(myURL.openStream());
            Element racine = doc.getDocumentElement();
            NodeList listeMed = racine.getElementsByTagName("Departement");
            for (int i = 0; i < listeMed.getLength(); i++) {
                Node medecin = listeMed.item(i);
                NodeList lesProprietes = medecin.getChildNodes();
                for (int j = 0; j < lesProprietes.getLength(); j++) {
                    if (lesProprietes.item(j).getNodeName().equals("num")) {
                        lesDepartements.add(lesProprietes.item(j).getTextContent().trim());
                    }
                }
            }
        } catch (SAXException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lesDepartements;
    }

    public static List<Medecin> getMedecins(String dep) {

        String url = "http://gaemedecins.appspot.com/Controleur/medParDep/listeMed/" + dep;
        URL myURL;
        List<Medecin> lesMedecins = new ArrayList<Medecin>();
        Document doc;
        
        //Ca c'est pour la création du medecin
        String nom = null, prenom = null, adresse = null, specialite = null, tel = null;
        Medecin med;

        try {
            myURL = new URL(url);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(myURL.openStream());
            Element racine = doc.getDocumentElement();
            NodeList listeMed = racine.getElementsByTagName("Medecin");
            
            for (int i = 0; i < listeMed.getLength(); i++) {
                Node medecin = listeMed.item(i);
                NodeList lesProprietes = medecin.getChildNodes();
                
                for (int j = 0; j < lesProprietes.getLength(); j++) {
                    if (lesProprietes.item(j).getNodeName().equals("nom")) {
                        nom = lesProprietes.item(j).getTextContent().trim();
                    }else if (lesProprietes.item(j).getNodeName().equals("prenom")) {
                        prenom = lesProprietes.item(j).getTextContent().trim();
                    }else if (lesProprietes.item(j).getNodeName().equals("adresse")) {
                        adresse = lesProprietes.item(j).getTextContent().trim();
                    }else if (lesProprietes.item(j).getNodeName().equals("specialite")) {
                        specialite = lesProprietes.item(j).getTextContent().trim();
                    }else if (lesProprietes.item(j).getNodeName().equals("tel")) {
                        tel = lesProprietes.item(j).getTextContent().trim();
                    }
                    
                }
                med = new Medecin(nom, prenom, adresse, specialite, tel);
                lesMedecins.add(med);
            }
        } catch (SAXException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lesMedecins;
    }
}
