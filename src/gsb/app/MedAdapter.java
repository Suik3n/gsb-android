/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gsb.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import model.Medecin;

/**
 *
 * @author Suiken
 */
public class MedAdapter extends BaseAdapter{
    
    private List<Medecin> lesMedecins;
    private Context c;
    
    MedAdapter(List<Medecin> data, Context c){
        lesMedecins = data;
        this.c = c;
    }
    
    public int getCount() {
        return lesMedecins.size();
    }

    public Object getItem(int i) {
        return lesMedecins.get(i);
    }

    public long getItemId(int i) {
        return i;
    }

    public View getView(int i, View v, ViewGroup vg) {
        LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = vi.inflate(R.layout.listefiltre, null);
        TextView nom = (TextView) v.findViewById(R.id.nom);
        TextView prenom = (TextView) v.findViewById(R.id.prenom);
        TextView adresse = (TextView) v.findViewById(R.id.adresse);
        TextView tel = (TextView) v.findViewById(R.id.tel);
        TextView specialite = (TextView) v.findViewById(R.id.specialite);
        
        Medecin leMed = lesMedecins.get(i);
        nom.setText("Nom : " + leMed.getNom());
        prenom.setText("Prénom : " + leMed.getPrenom());
        adresse.setText("Adresse : " + leMed.getAdresse());
        tel.setText("Téléphone : " + leMed.getTel());
        
        if(leMed.getSpecialite() == ""){
            leMed.setSpecialite("Non renseigné");
        }
        specialite.setText("Spécialité : " + leMed.getSpecialite());
        
        return v;
    }
    
}
