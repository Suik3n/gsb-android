/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gsb.app;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import model.DAO;
import model.Medecin;

/**
 *
 * @author Suiken
 */
public class MedecinsActivity extends ListActivity{
    
    private List<Medecin> lesMedecins;
    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        
        Intent inter = getIntent();
        String dep = inter.getStringExtra("leDep");
        
        this.lesMedecins = DAO.getMedecins(dep);

        MedAdapter adapter = new MedAdapter(lesMedecins, this);
        setListAdapter(adapter);
    }
}
