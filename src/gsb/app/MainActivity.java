package gsb.app;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import model.DAO;

public class MainActivity extends ListActivity {

    private List<String> lesDepartements;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        this.lesDepartements = DAO.getDepartement();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lesDepartements);
        setListAdapter(adapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent inter = new Intent(this, FiltreActivity.class); //A changer par MedecinsActivity si on veut reprendre l'ancienne version
        inter.putExtra("leDep", lesDepartements.get(position));
        startActivity(inter);
    }
}