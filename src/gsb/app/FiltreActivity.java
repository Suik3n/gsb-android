/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gsb.app;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import model.DAO;
import model.Medecin;

/**
 *
 * @author Suiken
 */
public class FiltreActivity extends ListActivity{
    private List<Medecin> lesMedecins;
    
    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        setContentView(R.layout.filtre);
        
        //Je récupère le département
        Intent inter = getIntent();
        String dep = inter.getStringExtra("leDep");
        this.lesMedecins = DAO.getMedecins(dep);
        
        MedAdapter adapter = new MedAdapter(lesMedecins, this);
        setListAdapter(adapter);
        
        //Programmation de ce que fait le bouton
        Button button = (Button) findViewById(R.id.buttonFiltre);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                //Je récupère le texte de l'input
                EditText inputFiltre = (EditText) findViewById(R.id.filtre);
                String filtre = inputFiltre.getText().toString();
                
                //Je crée la liste dans laquelle je vais foutre tout les médecins qui y seront
                List<Medecin> medecinsFiltres = new ArrayList<Medecin>();
                
                //Je parcoure la liste de base pour mettre tout les medecins filtrés
                for(Medecin unMedecin : lesMedecins){
                    if(unMedecin.getNom().startsWith(filtre)){
                        medecinsFiltres.add(unMedecin);
                    }
                }
                
                //J'affiche le tout
                MedAdapter adapter = new MedAdapter(medecinsFiltres, getApplicationContext());
                setListAdapter(adapter);
            }
        });
    }
}